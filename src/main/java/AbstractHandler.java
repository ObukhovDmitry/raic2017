import model.Player;
import model.World;

abstract class AbstractHandler implements IHandler, ISynchronized {

    protected GameStage gameStage;
    protected MoveMonitor moveMonitor;
    protected World world;
    protected MyMove move;
    protected Army army;
    protected double resourcesToMakeMove;

    @Override
    public void setMoveMonitor(MoveMonitor moveMonitor) {
        this.moveMonitor = moveMonitor;
    }

    @Override
    public void setGameStage(GameStage gameStage) {
        this.gameStage = gameStage;
    }

    boolean isEnemyNuclearStrike(World world) {

        Player enemyPlayer = world.getOpponentPlayer();
        int nextStrikeTickIndex = enemyPlayer.getNextNuclearStrikeTickIndex();

        return nextStrikeTickIndex >= 0;
    }
}
