class GameStage {

    private State state;

    GameStage() {
        state = State.EARLY_GAME;
    }

    State getState() {
        return state;
    }

    void setState(State state) {
        this.state = state;
    }

    /**
     * Перечисление, содержащие стадии игры
     * Определяет используемую стратегию
     */
    enum State {

        /**
         * Раняя стадия игры - идет построение, истребители во всю работают
         */
        EARLY_GAME,

        /**
         * Средняя стадия игры - истребители и батальоны работают
         */
        MIDDLE_GAME
    }
}
