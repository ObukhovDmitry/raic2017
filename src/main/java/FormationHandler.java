import model.VehicleType;
import model.World;

class FormationHandler extends AbstractHandler {

    // +-----------+
    // | Constants |
    // +-----------+

    // 1.5, чтобы не было такого, что монитор занял и ждешь долго, чтобы ход продолжить
    private static final double RESOURCE_NEEDED_TO_MOVE = 1.5;

    // SQUEEZE
    private static final double SQUEEZE_SCALE_FACTOR = 0.1;

    // +-----------+
    // | Variables |
    // +-----------+

    private FormationProgress progress = FormationProgress.SELECT;
    private VehicleType[] vehicleTypes = {
            VehicleType.FIGHTER,
            VehicleType.HELICOPTER,
            VehicleType.IFV,
            VehicleType.ARRV,
            VehicleType.TANK};
    private int currentVehicleTypeId = 0;
    private Battalion currentBattalion;
    private boolean isSelected;
    private int squeezeStartTick;


    // +---------+
    // | Methods |
    // +---------+

    FormationHandler(Army army) {
        this.army = army;
    }

    @Override
    public void doAction(World world, MyMove move, double resourcesToMakeMove) {

        this.world = world;
        this.move = move;
        this.resourcesToMakeMove = resourcesToMakeMove;

        if (resourcesToMakeMove < 1) {
            return;
        }

        buildFormation();
    }

    @Override
    public double necessaryOfMove(World world, double resourcesToMakeMove) {
        return gameStage.getState() == GameStage.State.EARLY_GAME
                ? necessaryOfMove(resourcesToMakeMove) : 0;
    }

    @Override
    public boolean isFinished() {
        return progress == FormationProgress.COMPLETED;
    }

    private double necessaryOfMove(double resourcesToMakeMove) {
        return resourcesToMakeMove < RESOURCE_NEEDED_TO_MOVE
                ? 0 : 1;
    }

    private void buildFormation() {

        buildFormationStep();

        if (isSelected) {
            moveMonitor.setState(MoveMonitor.State.FORMATION);
        } else {
            moveMonitor.setState(MoveMonitor.State.FREE);
        }
    }

    private void buildFormationStep() {

        switch (progress) {
            case SELECT: {
                selectGroup(vehicleTypes[currentVehicleTypeId]);
                progress = FormationProgress.BIND;
                break;
            }
            case BIND: {
                bindSelectedGroup();
                progress = FormationProgress.SQUEEZE;
                break;
            }

            case SQUEEZE: {
                squeezeSelectedGroup();
                currentVehicleTypeId++;
                progress = currentVehicleTypeId == vehicleTypes.length
                        ? FormationProgress.COMPLETED : FormationProgress.SELECT;
                break;
            }
        }
    }

    private void selectGroup(VehicleType vehicleType) {

        move.selectAll(vehicleType);
        isSelected = true;
    }

    private void bindSelectedGroup() {

        currentBattalion = getSelectedBattalion();
        currentBattalion.setSpeed(MyGame.speedOfType(vehicleTypes[currentVehicleTypeId]));
        army.addMyBattalion(currentBattalion);
        move.bind(currentBattalion);
    }

    private void squeezeSelectedGroup() {

        move.scale(currentBattalion, currentBattalion.getCenter(), SQUEEZE_SCALE_FACTOR);
        isSelected = false;
    }

    private Battalion getSelectedBattalion() {

        Battalion battalion = new Battalion();

        for (MyVehicle vehicle : army.getMyVehicles()) {
            if (vehicle.isSelected()) {
                battalion.addVehicle(vehicle);
            }
        }
        battalion.update();

        return battalion;
    }


    enum FormationProgress {

        SELECT,
        BIND,
        SQUEEZE,
        COMPLETED
    }
}
