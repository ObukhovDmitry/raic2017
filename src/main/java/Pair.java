class Pair<T, S> {
    T first;
    S second;

    Pair(T first, S second) {
        this.first = first;
        this.second = second;
    }
}
