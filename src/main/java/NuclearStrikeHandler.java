import model.World;

import java.util.*;

class NuclearStrikeHandler extends AbstractHandler {

    // NECESSARY_COEFFICIENT - коэффициент необходимости хода
    private static final double NECESSARY_COEFFICIENT = 2.0;

    // VISION_RANGE_COEFFICIENT - для того, чтобы юниты не были на границе с заной нанесения удара
    //      и случайно не выбежали
    private static final double VISION_RANGE_COEFFICIENT = 0.8;

    // MIN_VISION_RANGE минимальная дистанция от батальона до точки нанесения ядерного удара,
    //      чтобы начать перебор техники, которая может нанести ядерный удар
    //      ( = максимальная дистанция обзора)
    private static final double MIN_VISION_RANGE = 120;

    // BORDER_IGNORE_WIDTH - в переборе будут учитываться крайние юниты
    private static final double BORDER_IGNORE_WIDTH = 10;

    private Pos target;
    private int vehicleId;
    private boolean isNuclearAvailable;

    NuclearStrikeHandler(Army army) {
        this.army = army;
    }

    @Override
    public void doAction(World world, MyMove move, double resourcesToMakeMove) {

        this.world = world;
        this.move = move;
        this.resourcesToMakeMove = resourcesToMakeMove;

        if (isNuclearAvailable && resourcesToMakeMove > 1) {
            move.nuclearStrike(target, vehicleId);
            System.out.println("[TICK INDEX = " + world.getTickIndex() + "] NUCLEAR STRIKE");
        }
    }

    @Override
    public double necessaryOfMove(World world, double resourcesToMakeMove) {
        return isNuclearAvailable(world) ? NECESSARY_COEFFICIENT : 0;
    }

    @Override
    public boolean isFinished() {
        return false;
    }

    private boolean isNuclearAvailable(World world) {

        isNuclearAvailable = false;
        if (world.getMyPlayer().getRemainingNuclearStrikeCooldownTicks() > 0) {
            return false;
        }

        // определить точки по которым наносить удар выгоднее всего
        List<Pos> attackPoses = findAttackPos();

        // проверить достижимость своими юнитам
        for (Pos attackPos : attackPoses) {

            for (Battalion battalion : army.getMyBattalions()) {

                if (battalion.distTo(attackPos) > MIN_VISION_RANGE) {
                    continue;
                }

                double distToAttackPos = battalion.distTo(attackPos);

                for (MyVehicle vehicle : battalion.getUnits()) {

                    // пропускаем юниты, близкие к краю
                    if (attackPos.distanceTo(vehicle.getX(), vehicle.getY()) <
                            BORDER_IGNORE_WIDTH + distToAttackPos) {
                        continue;
                    }

                    Pos me = new Pos(vehicle.getX(), vehicle.getY());
                    int tileX = (int) (vehicle.getX() / MyGame.instance.getTerrainWeatherMapColumnCount());
                    int tileY = (int) (vehicle.getY() / MyGame.instance.getTerrainWeatherMapRowCount());
                    double visionRange =  VISION_RANGE_COEFFICIENT *
                            MyGame.visionRangeOfType(vehicle.getType()) *
                            MyGame.getVisionRangeCoefficientOfType(
                                    vehicle.getType(),
                                    world.getTerrainByCellXY()[tileX][tileY],
                                    world.getWeatherByCellXY()[tileX][tileY]);
                    if (me.distanceTo(attackPos) < visionRange) {
                        target = attackPos;
                        vehicleId = vehicle.getUniqueId();
                        isNuclearAvailable = true;
                        return true;
                    }
                }
            }
        }

        return false;
    }

    private List<Pos> findAttackPos() {

        army.getOpBattalions().sort(Collections.reverseOrder(Comparator.comparingInt(Battalion::getCount)));
        List<Pos> result = new ArrayList<>(army.getOpBattalions().size());

        for (Battalion enemy : army.getOpBattalions()) {

            // Если попадает по мне attackMe = true
            boolean attackMe = false;

            for (Battalion me : army.getMyBattalions()) {
                if (me.getCount() < 10) {
                    continue;
                }
                double dT = MyGame.instance.getTacticalNuclearStrikeDelay();
                double S = dT * me.getSpeed();
                Vector2D CT = new Vector2D(me.getCenter(), me.getTarget());
                Vector2D shift = CT.length() > S ? CT.norm(S) : CT;
                Pos target = me.getCenter().shift(shift);

                if (target.distanceTo(enemy.getCenter()) < MyGame.instance.getTacticalNuclearStrikeRadius()) {
                    attackMe = true;
                }
            }

            if (!attackMe) {
                result.add(enemy.getCenter());
            }
        }

        return result;
    }
}
