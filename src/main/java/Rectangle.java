class Rectangle implements Selectable {

    private double left;
    private double top;
    private double right;
    private double bottom;

    Rectangle(double left, double top, double right, double bottom) {
        this.left = left;
        this.top = top;
        this.right = right;
        this.bottom = bottom;
    }

    @Override
    public Pos getCenter() {
        return new Pos((left + right) / 2, (top + bottom) / 2);
    }

    @Override
    public double getRadius() {

        double a = right - left;
        double b = bottom - top;

        return Math.sqrt(a * a + b * b) / 2;
    }

    @Override
    public double getLeft() {
        return left;
    }

    @Override
    public double getTop() {
        return top;
    }

    @Override
    public double getRight() {
        return right;
    }

    @Override
    public double getBottom() {
        return bottom;
    }

    boolean contains(Pos p) {
        return p.x > left && p.x < right && p.y > top && p.y < bottom;
    }
}
