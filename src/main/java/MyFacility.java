import model.FacilityType;
import model.VehicleType;

class MyFacility extends Rectangle implements ISource {

    private static final double SIDE_SIZE = MyGame.instance.getFacilityWidth();
    // Формула имеент вид FACILITY_POTENTIAL_COEFFICIENT / (r + FACILITY_POTENTIAL_VALUE_0)
    private static final double FACILITY_VF_POTENTIAL_COEFFICIENT = 0.12;
    private static final double FACILITY_CC_POTENTIAL_COEFFICIENT = 0.1;
    private static final double FACILITY_POTENTIAL_VALUE_0 = 10;
    // MIN_DISTANCE_TO_FACILITY Нужен, чтобы не было деления на 0 в формуле вычисления потенциала
    private static final double MIN_DISTANCE_TO_FACILITY = 2;

    private int uniqueId;
    private int playerId;
    private double capturePoints;
    private double progress;
    private FacilityType type;
    private VehicleType vehicleType;
    private boolean isAttacked = false;

    MyFacility(int uniqueId, int playerId, double capturePoints, double progress, FacilityType type, VehicleType vehicleType, double left, double top) {

        super(left, top, left + SIDE_SIZE, top + SIDE_SIZE);
        this.uniqueId = uniqueId;
        this.playerId = playerId;
        this.capturePoints = capturePoints;
        this.progress = progress;
        this.type = type;
        this.vehicleType = vehicleType;
    }

    // Потенциал сооружений считается в BattalionHandler
    @Override
    public double calcPotential(long playerId, Battalion battalion, Pos target) {
        throw new UnsupportedOperationException();
        /*
        if (battalion.isAerial()) {
            return 0;
        }

        double captureCoefficient = (this.playerId == playerId) ?
                1.0 - Math.abs(capturePoints) / MyGame.instance.getMaxFacilityCapturePoints() :
                1.0;
        double coefficient = type == FacilityType.VEHICLE_FACTORY ?
                FACILITY_VF_POTENTIAL_COEFFICIENT :
                FACILITY_CC_POTENTIAL_COEFFICIENT;
        double distanceToFacility = Math.max(MIN_DISTANCE_TO_FACILITY, target.distanceTo(getCenter()));
        double distanceToFacilityWithPenalty = distanceToFacility + FACILITY_POTENTIAL_VALUE_0;


        return captureCoefficient * coefficient * battalion.getCount() / distanceToFacilityWithPenalty;*/
    }

    int getUniqueId() {
        return uniqueId;
    }

    int getPlayerId() {
        return playerId;
    }

    double getCapturePoints() {
        return capturePoints;
    }

    double getProgress() {
        return progress;
    }

    double getMaxProgress() {

        switch (vehicleType) {
            case ARRV:
                return MyGame.instance.getArrvProductionCost();

            case FIGHTER:
                return MyGame.instance.getFighterProductionCost();

            case HELICOPTER:
                return MyGame.instance.getHelicopterProductionCost();

            case IFV:
                return MyGame.instance.getIfvProductionCost();

            case TANK:
                return MyGame.instance.getTankProductionCost();
        }

        throw new UnsupportedOperationException();
    }

    FacilityType getType() {
        return type;
    }

    VehicleType getVehicleType() {
        return vehicleType;
    }

    double getSideSize() {
        return SIDE_SIZE;
    }

    boolean isAttacked() {
        return isAttacked;
    }

    void setProperties(int ownerPlayerId, double capturePoints, double progress, VehicleType vehicleType) {

        isAttacked = this.capturePoints > capturePoints;
        this.playerId = ownerPlayerId;
        this.capturePoints = capturePoints;
        this.progress = progress;
        this.vehicleType = vehicleType;
    }

    boolean contain(MyVehicle vehicle) {
        return super.contains(new Pos(vehicle.getX(), vehicle.getY()));
    }
}
