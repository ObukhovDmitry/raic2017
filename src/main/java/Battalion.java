import model.VehicleType;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class Battalion implements ISource {

    /**
     * POTENTIAL_ALLY_REPEL_COEFFICIENT - коэффициент при учете союзного потенциала (стоит при 1/e^r)
     * POTENTIAL_ALLY_WEAK_REPEL_COEFFICIENT ~ 1/e^r
     * POTENTIAL_ALLY_HEAL_COEFFICIENT ~ n/r^2
     * POTENTIAL_ALLY_PROTECT_COEFFICIENT ~ n^2/r^2
     */
    private static final double POTENTIAL_ALLY_REPEL_COEFFICIENT = 2;
    private static final double POTENTIAL_ALLY_WEAK_REPEL_COEFFICIENT = 0.7;
    private static final double POTENTIAL_ALLY_HEAL_COEFFICIENT = 1.5;
    private static final double POTENTIAL_ALLY_HEAL_VALUE_0 = 30;
    private static final double POTENTIAL_ALLY_PROTECT_COEFFICIENT = 0.00007;
    private static final double POTENTIAL_ALLY_PROTECT_VALUE_0 = 100;

    /**
     * UNIT_RADIUS чтобы не было деленья на 0 при вычислении потенциала
     *                                  ~ по сути это радиус юнита
     */
    private static final double UNIT_RADIUS = 2;

    /*
     * ALLOWABLE_WEAK_PERCENT
     */
    private static final double ALLOWABLE_WEAK_PERCENT = 0.09;
    /*
     * Коэффициенты для определения нарушения построения батальона
     */
    static final double OUTER_REGROUP_COEFFICIENT = 20;
    static final double INNER_REGROUP_COEFFICIENT = 14;

    // Уникальный идентификатор
    private int uniqueId;

    // Состав батальона
    private List<MyVehicle> units;
    private int[] countOfType;

    // Общее здоровье юнитов батальона
    private double totalDurability;

    // Дефствие на карте
    private Pos target;
    private Pos preTarget;
    private double speed;

    // Форма батальона
    private Ellipse core;

    // Доп. хар-ки
    private Pos weakCenter;
    private int numberOfWeakUnits;

    // Статус батальона
    private boolean isAbstract = false;
    private State state = State.IDLE;
    private TargetState targetState = TargetState.ATTACK;

    Battalion() {
        units = new ArrayList<>();
        core = new Ellipse();
        countOfType = new int[VehicleType.values().length];
    }

    // getters
    List<MyVehicle> getUnits() {
        return units;
    }
    int getUniqueId() {
        return uniqueId;
    }
    int getCount() {
        return isAbstract ? Arrays.stream(countOfType).sum() : units.size();
    }
    int getCountOfType(VehicleType type) {
        return countOfType[type.ordinal()];
    }
    double getTotalDurability() {
        return totalDurability;
    }
    double getSpeed() {
        return speed;
    }
    Ellipse getCore() {
        return core;
    }
    Pos getCenter() {
        return core.getCenter();
    }
    Pos getTarget() {
        return target == null ? core.getCenter() : target;
    }
    Pos getPreTarget() {
        return preTarget == null ? core.getCenter() : preTarget;
    }
    State getState() {
        return state;
    }
    TargetState getTargetState() {
        return targetState;
    }
    boolean isAerial() {
        return countOfType[VehicleType.FIGHTER.ordinal()] > 0 ||
                countOfType[VehicleType.HELICOPTER.ordinal()] > 0;
    }
    boolean isGround() {
        return countOfType[VehicleType.ARRV.ordinal()] > 0 ||
                countOfType[VehicleType.IFV.ordinal()] > 0 ||
                countOfType[VehicleType.TANK.ordinal()] > 0;
    }

    // setters
    void addVehicle(MyVehicle vehicle) {
        units.add(vehicle);
        countOfType[vehicle.getType().ordinal()]++;
    }
    void setUniqueId(int uniqueId) {
        this.uniqueId = uniqueId;
    }
    void setTarget(Pos target) {
        this.target = target;
    }
    void setPreTarget(Pos target) {
        this.preTarget = target;
    }
    void setSpeed(double speed) {
        this.speed = speed;
    }
    void setState(State state) {
        this.state = state;
    }
    void setTargetState(TargetState targetState) {
        this.targetState = targetState;
    }

    // other methods
    double distTo(Pos p) {

        Vector2D fromCenterToP = new Vector2D(core.getCenter(), p).norm();
        double distToBorder = core.getRadiusForDirection(fromCenterToP);
        double dist = core.getCenter().distanceTo(p) - distToBorder;

        return dist > 0 ? dist : 0;
    }
    double distTo(Ellipse ellipse) {

        Vector2D direction = new Vector2D(core.getCenter(), ellipse.getCenter()).norm();
        double borderEllipse = ellipse.getRadiusForDirection(direction);
        double borderThis = core.getRadiusForDirection(direction);
        double dist = core.getCenter().distanceTo(ellipse.getCenter()) - borderEllipse - borderThis;

        return dist > 0 ? dist : 0;
    }
    double getMaxRadius() {
        return core.getR1() + UNIT_RADIUS;
    }

    // ОБНОВЛЕНИЕ КОНТЕНТА
    // Если ни одна координата юнитов не изменилась, то присваивается State IDLE
    void checkIdleState() {

        if (state == State.DEAD) {
            return;
        }
        for (MyVehicle unit: units) {
            if (unit.isCoordinatesUpdated()) {
                return;
            }
        }
        state = State.IDLE;
    }
    void removeVehicle(MyVehicle unit) {

        if (units.remove(unit)) {
            countOfType[unit.getType().ordinal()]--;
        }
        if (units.size() == 0) {
            state = State.DEAD;
        }
    }
    void update() {

        updateCenter();
        updateWeakCenter();
        updateRadii();
        updateDurability();
    }
    private void updateCenter() {

        Pos newCenter = new Pos();

        for (MyVehicle unit : units) {
            newCenter.x += unit.getX();
            newCenter.y += unit.getY();
        }
        newCenter.x /= units.size();
        newCenter.y /= units.size();

        core.setCenter(newCenter);
    }
    private void updateWeakCenter() {

        weakCenter = new Pos();
        numberOfWeakUnits = 0;

        for (MyVehicle unit : units) {
            if (unit.getDurability() == unit.getMaxDurability()) {
                continue;
            }
            numberOfWeakUnits++;
            weakCenter.x += unit.getX();
            weakCenter.y += unit.getY();
        }
        if (numberOfWeakUnits == 0) {
            weakCenter = core.getCenter();
        } else {
            weakCenter.x /= numberOfWeakUnits;
            weakCenter.y /= numberOfWeakUnits;
        }
    }
    private void updateRadii() {

        // особый случай, если только один юнит
        if (units.size() == 0) {
            core.setV1(new Vector2D(0, 1));
            core.setV2(new Vector2D(1, 0));
            core.setR1(0);
            core.setR2(0);
            return;
        }

        // находим самого дальнего юнита
        double maxDistFromCenterToUnit = 0;
        Pos theFurthest = new Pos();

        for (MyVehicle unit : units) {
            Pos current = new Pos(unit.getX(), unit.getY());
            double currentDist = current.distanceTo(core.getCenter());

            if (currentDist > maxDistFromCenterToUnit) {
                maxDistFromCenterToUnit = currentDist;
                theFurthest = current;
            }
        }

        // направляющие вектора для радиусов
        core.setV1(new Vector2D(core.getCenter(), theFurthest).norm());
        core.setV2(new Vector2D(-core.getV1().getY(), core.getV1().getX()));

        // первый радиуса - до самого дальнего юнита
        core.setR1(maxDistFromCenterToUnit);

        // находим второй радиус (обозначения см. формулу в тетрадке)
        double r1 = maxDistFromCenterToUnit;
        maxDistFromCenterToUnit = 0;
        for (MyVehicle unit : units) {
            Pos current = new Pos(unit.getX(), unit.getY());
            Vector2D fromCenterToCurrent = new Vector2D(core.getCenter(), current).norm();
            double cosA = core.getV1().dotprod(fromCenterToCurrent);
            double sinA = Math.sqrt(1 - cosA * cosA);
            // вдоль v1 не рассматриваем
            if (Math.abs(sinA) < 0.5) {
                continue;
            }
            double r = current.distanceTo(core.getCenter());
            double r2 = Math.abs(r * r1 * sinA) /
                    Math.sqrt(r1 * r1 - r * r * cosA * cosA);

            if (r2 > maxDistFromCenterToUnit) {
                maxDistFromCenterToUnit = r2;
            }
        }
        core.setR2(maxDistFromCenterToUnit);
    }
    private void updateDurability() {

        totalDurability = 0;

        for (MyVehicle unit : units) {
            totalDurability += unit.getDurability();
        }
    }

    // ПРОВЕРКА СОСТОЯНИЙ
    // todo v65 мб дополнительно учитывать соотношение сторон
    boolean isDivided() {

        double volume = core.getR1() * core.getR2();
        double maxVolumeCoefficient = targetState == TargetState.REGROUP ?
                INNER_REGROUP_COEFFICIENT :
                OUTER_REGROUP_COEFFICIENT;
        double maxVolume = UNIT_RADIUS * UNIT_RADIUS + maxVolumeCoefficient * units.size();

        return volume > maxVolume;
    }
    boolean isWeak() {

        int numberOfWeakUnits = 0;
        for (MyVehicle vehicle : units) {
            if (vehicle.getDurability() < vehicle.getMaxDurability()) {
                numberOfWeakUnits++;
            }
        }
        return numberOfWeakUnits > ALLOWABLE_WEAK_PERCENT * units.size();
    }

    // Проверка, что в момент бандинга все ок
    void checkSelect() {
        // Удаляем те, которые по какой-то причине не выделились
        units.removeIf(v -> !v.isSelected());

        updateCountOfType();
        if (units.size() == 0) {
            state = State.DEAD;
        }
        // Чужие юниты, которые случайно выделились, будут удалены при следующем их обновлении
    }
    private void updateCountOfType() {
        for (int i = 0; i < countOfType.length; i++) {
            countOfType[i] = 0;
        }
        for (MyVehicle unit : units) {
            countOfType[unit.getType().ordinal()]++;
        }
    }

    // +------------+
    // | ПОТЕНЦИАЛЫ |
    // +------------+

    @Override
    // TODO v55 Костыль 1 - мои, 2 - противник
    public double calcPotential(long playerId, Battalion battalion, Pos target) {
        return playerId == 1 ? calcAllyPotential(battalion, target) : calcEnemyPotential(battalion, target);
    }

    // ВЫЧИСЛЕНИЕ ПОТЕНЦИАЛА СОЮЗНИКОВ
    private double calcAllyPotential(Battalion battalion, Pos target) {

        if (uniqueId == battalion.getUniqueId()) {
            return 0;
        }

        return canIntersect(battalion) ?
                -calcAllyRepelProfit(battalion, target) :
                calcAllyProtectProfit(battalion, target) + calcAllyHealProfit(battalion, target);
    }
    private double calcAllyProtectProfit(Battalion battalion, Pos target) {

        // истребители притягиваются к хилерам и танкам
        // вертолеты притяггиваются к ифв

        // определяем где будет находиться союзник (то есть this)
        Vector2D battalionShift = new Vector2D(battalion.getCenter(), target);
        double tickCount = battalionShift.length() / battalion.speed;
        Vector2D otherRestOfWay = target == null ? new Vector2D(0, 0) : new Vector2D(core.getCenter(), target);
        Vector2D otherShift = otherRestOfWay.length() > speed * tickCount ?
                otherRestOfWay.norm(speed * tickCount) : otherRestOfWay;
        Pos otherCenter = core.getCenter().shift(otherShift);


        double coefficient =
                battalion.countOfType[VehicleType.FIGHTER.ordinal()] *
                (countOfType[VehicleType.ARRV.ordinal()] + countOfType[VehicleType.TANK.ordinal()]) +
                battalion.countOfType[VehicleType.HELICOPTER.ordinal()] *
                countOfType[VehicleType.IFV.ordinal()];
        double distanceBetweenCenters = Math.max(UNIT_RADIUS, otherCenter.distanceTo(target));
        double distanceWithPenalty = distanceBetweenCenters + POTENTIAL_ALLY_PROTECT_VALUE_0;

        return coefficient * POTENTIAL_ALLY_PROTECT_COEFFICIENT / (distanceWithPenalty * distanceWithPenalty);
    }
    private double calcAllyHealProfit(Battalion whom, Pos whomTarget) {

        int numberOfActualHealers = Math.min(
                whom.numberOfWeakUnits,
                countOfType[VehicleType.ARRV.ordinal()]);

        if (numberOfActualHealers == 0 || !whom.isWeak()) {
            return 0;
        }

        // определяем где будет находиться раненное место группы
        Vector2D whomShift = new Vector2D(whom.getCenter(), whomTarget);
        Pos whomWeakCenter = whom.weakCenter.shift(whomShift);

        // определяем где будет находиться союзник (то есть this)
        double tickCount = whomShift.length() / whom.speed;
        Vector2D otherRestOfWay = target == null ? new Vector2D(0, 0) : new Vector2D(core.getCenter(), target);
        Vector2D otherShift = otherRestOfWay.length() > speed * tickCount ?
                otherRestOfWay.norm(speed * tickCount) : otherRestOfWay;
        Pos otherCenter = core.getCenter().shift(otherShift);

        double distanceFromWeakToHealCenter = Math.max(UNIT_RADIUS, otherCenter.distanceTo(whomWeakCenter));
        double distanceWithPenalty = distanceFromWeakToHealCenter + POTENTIAL_ALLY_HEAL_VALUE_0;

        return POTENTIAL_ALLY_HEAL_COEFFICIENT * numberOfActualHealers /
                (distanceWithPenalty * distanceWithPenalty);
    }
    private double calcAllyRepelProfit(Battalion battalion, Pos whomTarget) {

        double tickCount = new Vector2D(battalion.getCenter(), whomTarget).length() / battalion.speed;

        Vector2D thisRestOfWay = target == null ? new Vector2D(0, 0) : new Vector2D(core.getCenter(), target);
        Vector2D thisShift = thisRestOfWay.length() > speed * tickCount ?
                thisRestOfWay.norm(speed * tickCount) : thisRestOfWay;
        Pos thisCenter = core.getCenter().shift(thisShift);

        Vector2D fromBatCenterToThisCenter = new Vector2D(whomTarget, thisCenter);
        double lengthOfRadii = core.getRadiusForDirection(fromBatCenterToThisCenter)
                + battalion.core.getRadiusForDirection(fromBatCenterToThisCenter);
        //double dist = Math.max(UNIT_RADIUS, fromBatCenterToThisCenter.length() - lengthOfRadii);
        // так даже лучше, пусть и появляется иногда negative infinity
        double dist = fromBatCenterToThisCenter.length() - lengthOfRadii;

        double allyRepelCoefficient = calcAllyRepelCoefficient(battalion);
        return Math.exp((-dist) / allyRepelCoefficient);
    }
    private boolean canIntersect(Battalion battalion) {
        return (battalion.isAerial() && isAerial()) || (battalion.isGround() && isGround());
    }
    // Strategy (weak or not)
    private double calcAllyRepelCoefficient(Battalion battalion) {
        return battalion.isWeak() ? POTENTIAL_ALLY_WEAK_REPEL_COEFFICIENT : POTENTIAL_ALLY_REPEL_COEFFICIENT;
    }

    // ВЫЧИСЛЕНИЕ ПОТЕНЦИАЛА ПРОТИВНИКА
    // Оно определено в BattalionHandler
    private double calcEnemyPotential(Battalion me, Pos target) {
        throw new UnsupportedOperationException();
    }


    // АБСТРАКТНЫЙ БАТАЛЬОН (для вычисления общей силы)
    void setAbstract(int opf, int oph, int opa, int opi, int opt) {

        isAbstract = true;

        countOfType = new int[VehicleType.values().length];
        countOfType[VehicleType.FIGHTER.ordinal()] = opf;
        countOfType[VehicleType.HELICOPTER.ordinal()] = oph;
        countOfType[VehicleType.ARRV.ordinal()] = opa;
        countOfType[VehicleType.IFV.ordinal()] = opi;
        countOfType[VehicleType.TANK.ordinal()] = opt;

        totalDurability = Arrays.stream(countOfType).sum() * 100;
    }


    enum State {
        IDLE,
        MOVE,
        SCALE,
        ROTATE,
        DEAD
    }
    enum TargetState {
        ATTACK,
        DODGE_NUCLEAR,
        REGROUP_NUCLEAR,
        REGROUP
    }
}
