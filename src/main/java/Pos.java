class Pos {
    double x = 0;
    double y = 0;

    Pos() {}

    Pos(double x, double y) {
        this.x = x;
        this.y = y;
    }

    double distanceTo(Pos p) {
        return distanceTo(p.x, p.y);
    }

    double distanceTo(double px, double py) {
        return Math.sqrt((px - x) * (px - x) + (py - y) * (py - y));
    }

    Pos shift(Vector2D direction) {
        return new Pos(x + direction.getX(), y + direction.getY());
    }

    @Override
    public String toString() {
        return "x=" + x + ", y=" + y;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Pos pos = (Pos) o;

        if (Double.compare(pos.x, x) != 0) return false;
        return Double.compare(pos.y, y) == 0;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        temp = Double.doubleToLongBits(x);
        result = (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(y);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }
}
