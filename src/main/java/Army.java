import model.*;

import java.util.*;

class Army {

    // кол-во юнитов каэдого типа на старте
    private static final int START_UNIT_COUNT = 500;
    private static final int START_UNIT_COUNT_OF_EACH_TYPE = 100;

    // Списки техники, initialCapacity = 1024
    private List<MyVehicle> myVehicles;
    private List<MyVehicle> opVehicles;

    // Кол-во техники
    private int[] myCountByType;
    private boolean[] opVehicleRepeated;
    private int opCount;
    private int[] opCountByType;

    // Списки групп
    private List<Battalion> myBattalions;
    private List<Battalion> opBattalions;
    private Map<VehicleType, List<Battalion>> opBattalionsByType;

    // Список сооружений
    private List<MyFacility> facilities;

    // Список стен
    private List<Wall> walls;

    //Текущая техника
    private Battalion current;

    // nextBind - счетчик, для байндинга батальонов
    private int nextBind = 1;

    Army() {

        walls = new ArrayList<>(4);
        walls.add(Wall.LEFT);
        walls.add(Wall.TOP);
        walls.add(Wall.RIGHT);
        walls.add(Wall.BOTTOM);
        walls.add(Wall.CENTER);
    }

    int nextBind() {
        return nextBind++;
    }

    // ГЕТТЕРЫ
    int myUnitsCount() {
        return myVehicles.size();
    }
    int myUnitsCountOf(VehicleType type) {
        return myCountByType[type.ordinal()];
    }
    int opUnitsCount() {
        return opCount;
    }
    int opUnitsCountOf(VehicleType type) {
        return opCountByType[type.ordinal()];
    }
    List<MyVehicle> getMyVehicles() {
        return myVehicles;
    }
    List<MyVehicle> getOpVehicles() {
        return opVehicles;
    }
    List<Battalion> getMyBattalions() {
        return myBattalions;
    }
    List<Battalion> getOpBattalions() {
        return opBattalions;
    }
    List<Battalion> getOpBattalionOf(VehicleType type) {
        return opBattalionsByType.get(type);
    }
    List<MyFacility> getFacilities() {
        return facilities;
    }
    List<Wall> getWalls() {
        return walls;
    }
    MyFacility getFacilityById(int facilityId) {
        for (MyFacility facility : facilities) {
            if (facility.getUniqueId() == facilityId) {
                return facility;
            }
        }

        return null;
    }

    // ТЕКУЩИЙ
    Battalion getCurrent() {
        return current;
    }
    void setCurrent(Battalion current) {
        this.current = current;
    }

    // ТЕХНИКА
    void initVehicles(World world) {

        myVehicles = new ArrayList<>(1024);
        opVehicles = new ArrayList<>(1024);

        opCount = START_UNIT_COUNT;
        opCountByType = new int[VehicleType.values().length];
        myCountByType = new int[VehicleType.values().length];
        for (int i = 0; i < VehicleType.values().length; i++) {
            opCountByType[i] = START_UNIT_COUNT_OF_EACH_TYPE;
            myCountByType[i] = START_UNIT_COUNT_OF_EACH_TYPE;
        }
        opVehicleRepeated = new boolean[START_UNIT_COUNT];

        Vehicle[] vehiclesArray = world.getNewVehicles();
        for (Vehicle vehicleImage : vehiclesArray) {
            MyVehicle vehicle = new MyVehicle(
                    (int) vehicleImage.getId(),
                    (int) vehicleImage.getPlayerId(),
                    vehicleImage.getX(),
                    vehicleImage.getY(),
                    vehicleImage.getType(),
                    vehicleImage.getMaxDurability());
            if (vehicle.getPlayerId() == world.getMyPlayer().getId()) {
                myVehicles.add(vehicle);
            } else {
                opVehicles.add(vehicle);
            }
        }

        myVehicles.sort(Comparator.comparingInt(MyVehicle::getUniqueId));
        opVehicles.sort(Comparator.comparingInt(MyVehicle::getUniqueId));
    }
    void updateVehicles(World world) {

        updateNewVehicles(world);
        updateVehicles(myVehicles, world);
        updateVehicles(opVehicles, world);
    }
    private void updateNewVehicles(World world) {

        Vehicle[] vehiclesArray = world.getNewVehicles();
        for (Vehicle vehicleImage : vehiclesArray) {
            MyVehicle vehicle = new MyVehicle(
                    (int) vehicleImage.getId(),
                    (int) vehicleImage.getPlayerId(),
                    vehicleImage.getX(),
                    vehicleImage.getY(),
                    vehicleImage.getType(),
                    vehicleImage.getMaxDurability());
            if (vehicle.getPlayerId() == world.getMyPlayer().getId()) {
                myVehicles.add(vehicle);
                myCountByType[vehicle.getType().ordinal()]++;
                addNewVehicleToBattalion(vehicle);
            } else {
                opVehicles.add(vehicle);
                updateOpCount(vehicle);
            }
        }

        myVehicles.sort(Comparator.comparingInt(MyVehicle::getUniqueId));
        opVehicles.sort(Comparator.comparingInt(MyVehicle::getUniqueId));
    }
    private void updateVehicles(List<MyVehicle> vehicles, World world) {

        // получить упорядоченный по uniqueId список обновлений
        List<VehicleUpdate> vehicleUpdates = Arrays.asList(world.getVehicleUpdates());
        vehicleUpdates.sort(Comparator.comparingLong(VehicleUpdate::getId));

        // убрать обновление хода с прошлого тика
        for (MyVehicle vehicle : vehicles) {
            vehicle.setCoordinatesUpdated(false);
        }

        // обновление
        Iterator<MyVehicle> vehicleIterator = vehicles.iterator();
        Iterator<VehicleUpdate> updateIterator = vehicleUpdates.iterator();

        if (!updateIterator.hasNext() || !vehicleIterator.hasNext()) {
            return;
        }

        VehicleUpdate currentUpdate = updateIterator.next();
        MyVehicle currentVehicle = vehicleIterator.next();

        boolean firstIteration = true;
        boolean lastIteration = !vehicleIterator.hasNext() && !updateIterator.hasNext();
        while (vehicleIterator.hasNext() || updateIterator.hasNext() || firstIteration || lastIteration) {
            firstIteration = false;

            if (currentUpdate.getId() == currentVehicle.getUniqueId()) {
                // обновляем, либо удаляем
                if (currentUpdate.getDurability() == 0) {
                    removeVehicleFromBattalions(currentVehicle);
                    vehicleIterator.remove();

                    if (currentVehicle.getPlayerId() == world.getOpponentPlayer().getId()) {
                        opCount--;
                        opCountByType[currentVehicle.getType().ordinal()]--;
                    } else {
                        myCountByType[currentVehicle.getType().ordinal()]--;
                    }

                } else {
                    updateVehicle(currentVehicle, currentUpdate);
                }

                // next step
                currentVehicle = vehicleIterator.hasNext() ? vehicleIterator.next() : currentVehicle;
                currentUpdate = updateIterator.hasNext() ? updateIterator.next() : currentUpdate;

            } else if (currentUpdate.getId() > currentVehicle.getUniqueId() && vehicleIterator.hasNext()) {
                currentVehicle = vehicleIterator.next();

            } else if (currentUpdate.getId() < currentVehicle.getUniqueId() && updateIterator.hasNext()) {
                currentUpdate = updateIterator.next();

            } else {
                break;
            }

            if (!vehicleIterator.hasNext() && !updateIterator.hasNext()) {
                lastIteration = !lastIteration;
            }
        }
    }
    private void updateVehicle(MyVehicle vehicle, VehicleUpdate update) {

        if (Double.compare(vehicle.getX(), update.getX()) != 0 ||
                Double.compare(vehicle.getY(), update.getY()) != 0) {
            vehicle.setCoordinatesUpdated(true);
            vehicle.setX(update.getX());
            vehicle.setY(update.getY());
        }
        vehicle.setDurability(update.getDurability());
        vehicle.setSelected(update.isSelected());
        if (update.getGroups().length == 1) {
            vehicle.setBattalionId(update.getGroups()[0]);
        } else if (update.getGroups().length > 1) {
            removeVehicleFromBattalions(vehicle);
        }
    }
    private void removeVehicleFromBattalions(MyVehicle vehicle) {

        myBattalions.forEach(battalion -> battalion.removeVehicle(vehicle));
        myBattalions.removeIf(battalion -> battalion.getState() == Battalion.State.DEAD);

        opBattalions.forEach(battalion -> battalion.removeVehicle(vehicle));
        opBattalions.removeIf(battalion -> battalion.getState() == Battalion.State.DEAD);
    }
    private void addNewVehicleToBattalion(MyVehicle vehicle) {
        // определить, на каком сооружении появился юнит
        int facilityId = -1;
        VehicleType vehicleType = null;
        for (MyFacility facility : facilities) {
            if (facility.contain(vehicle)) {
                facilityId = facility.getUniqueId();
                vehicleType = facility.getVehicleType();
                break;
            }
        }
        if (facilityId == -1) {
            throw new UnsupportedOperationException();
        }

        // добавить в батальон новый юнит, если батальон уже есть
        for (Battalion battalion : myBattalions) {
            if (battalion.getUniqueId() == -facilityId) {
                battalion.addVehicle(vehicle);
                return;
            }
        }

        // создать новый батальон, если его нет
        Battalion battalion = new Battalion();
        battalion.setUniqueId(-facilityId);
        battalion.setSpeed(MyGame.speedOfType(vehicleType));
        battalion.addVehicle(vehicle);
        myBattalions.add(battalion);
    }
    private void updateOpCount(MyVehicle vehicle) {

        if (opVehicleRepeated[vehicle.getUniqueId() % START_UNIT_COUNT]) {
            opCount++;
            opCountByType[vehicle.getType().ordinal()]++;
        } else {
            opVehicleRepeated[vehicle.getUniqueId() % START_UNIT_COUNT] = true;
        }
    }

    // БАТАЛЬОНЫ, ИНИЦИАЛИЗАЦИЯ
    void initBattalions(World world) {
        initMyBattalions(world);
        initOpBattalions(world);
    }
    private void initMyBattalions(World world) {
        myBattalions = new ArrayList<>(16);
    }
    private void initOpBattalions(World world) {
        updateOpBattalions(world);
    }

    // БАТАЛЬОНЫ, ОБНОВЛЕНИЕ ПАРАМЕТРОВ
    void updateMyBattalions() {
        for (Battalion battalion : myBattalions) {
            battalion.checkIdleState();
            battalion.update();
        }
    }
    void addMyBattalion(Battalion battalion) {
        battalion.setUniqueId(nextBind++);
        myBattalions.add(battalion);
    }
    // v85 выключил обновление всех батальонов
    void updateOpBattalions(World world) {

        updateOpBattalionsImpl(world);

        /*
        opBattalionsByType = new EnumMap<>(VehicleType.class);
        updateOpBattalionOf(world, VehicleType.FIGHTER);
        updateOpBattalionOf(world, VehicleType.HELICOPTER);
        updateOpBattalionOf(world, VehicleType.IFV);
        updateOpBattalionOf(world, VehicleType.ARRV);
        updateOpBattalionOf(world, VehicleType.TANK);
        */
    }
    private void updateOpBattalionsImpl(World world) {

        ClusteringTilesBuilder tilesBuilder = new ClusteringTilesBuilder();
        ClusteringTiles tiles = tilesBuilder.build(world, opVehicles, v -> true);

        opBattalions = new ArrayList<>(tiles.getNumberOfGroups());
        double[] speed = new double[tiles.getNumberOfGroups()];

        for (int i = 0; i < tiles.getNumberOfGroups(); i++) {
            opBattalions.add(new Battalion());
            opBattalions.get(i).setUniqueId(i);
            speed[i] = Double.POSITIVE_INFINITY;
        }

        ClusteringTiles.Iterator iterator = tiles.iterator();
        while (iterator.hasNext()) {

            ClusteringTile currentTile = iterator.next();
            int currentGroupIndex = currentTile.getGroupId();

            for (Integer vehicleIndex : currentTile.getIndices()) {

                MyVehicle currentVehicle = opVehicles.get(vehicleIndex);

                opBattalions.get(currentGroupIndex).addVehicle(currentVehicle);
                speed[currentGroupIndex] = Math.min(speed[currentGroupIndex], MyGame.speedOfType(currentVehicle.getType()));
            }
        }
        for (int i = 0; i < tiles.getNumberOfGroups(); i++) {
            opBattalions.get(i).setSpeed(speed[i]);
            opBattalions.get(i).update();
        }
    }
    private void updateOpBattalionOf(World world, VehicleType type) {

        ClusteringTilesBuilder tilesBuilder = new ClusteringTilesBuilder();
        ClusteringTiles tiles = tilesBuilder.build(world, opVehicles, v -> v.getType() == type);

        List<Battalion> thisTypeEnemies = new ArrayList<>(tiles.getNumberOfGroups());
        for (int i = 0; i < tiles.getNumberOfGroups(); i++) {
            thisTypeEnemies.add(new Battalion());
        }
        ClusteringTiles.Iterator iterator = tiles.iterator();
        while (iterator.hasNext()) {
            ClusteringTile currentTile = iterator.next();
            for (Integer vehicleIndex : currentTile.getIndices()) {
                thisTypeEnemies.get(currentTile.getGroupId()).addVehicle(opVehicles.get(vehicleIndex));
            }
        }
        for (Battalion battalion : thisTypeEnemies) {
            battalion.update();
        }

        opBattalionsByType.put(type, thisTypeEnemies);
    }

    // СООРУЖЕНИЯ, ОБНОВЛЕНИЕ
    void initFacilities(World world) {

        facilities = new ArrayList<>();
        Facility[] facilityImages = world.getFacilities();

        for (Facility facilityImage : facilityImages) {
            facilities.add(new MyFacility(
                    (int) facilityImage.getId(),
                    (int) facilityImage.getOwnerPlayerId(),
                    facilityImage.getCapturePoints(),
                    facilityImage.getProductionProgress(),
                    facilityImage.getType(),
                    facilityImage.getVehicleType(),
                    facilityImage.getLeft(),
                    facilityImage.getTop()));
        }

        facilities.sort(Comparator.comparingInt(MyFacility::getUniqueId));
    }
    void updateFacilities(World world) {

        Facility[] facilityImages = world.getFacilities();

        for (Facility facilityImage : facilityImages) {
            int facilityId = (int) facilityImage.getId() - 1;
            MyFacility facility = facilities.get(facilityId);
            facility.setProperties(
                    (int) facilityImage.getOwnerPlayerId(),
                    facilityImage.getCapturePoints(),
                    facilityImage.getProductionProgress(),
                    facilityImage.getVehicleType());
        }
    }
}
