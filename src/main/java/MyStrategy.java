import model.*;

import java.util.LinkedList;

public final class MyStrategy implements Strategy {

    private World world;
    private Game game;
    private Move move;

    private MainHandler mainHandler;
    private Army army;
    private LinkedList<Integer> lastMoves = new LinkedList<>();

    public void move(Player me, World world, Game game, Move move) {

        this.world = world;
        this.game = game;
        this.move = move;
        MyGame.setGame(game);

        if (world.getTickIndex() == 0) {
            army = new Army();
            army.initVehicles(world);
            army.initBattalions(world);
            army.initFacilities(world);
            mainHandler = new MainHandler(army, game.getRandomSeed());
        } else {
            army.updateVehicles(world);
            army.updateMyBattalions();
            army.updateOpBattalions(world);
            army.updateFacilities(world);
        }

        move.setAction(ActionType.NONE);
        mainHandler.doAction(
                world,
                new MyMove(move, world),
                calcResourcesToMakeNextMove());
        updateLastMoves();
    }

    public void move(Player me, World world, Game game, Move move, ZWindow window) {

        this.world = world;
        this.game = game;
        this.move = move;
        MyGame.setGame(game);

        if (world.getTickIndex() == 0) {
            army = new Army();
            army.initVehicles(world);
            army.initBattalions(world);
            army.initFacilities(world);
            mainHandler = new MainHandler(army, game.getRandomSeed());
        } else {
            army.updateVehicles(world);
            army.updateMyBattalions();
            army.updateOpBattalions(world);
            army.updateFacilities(world);
        }

        move.setAction(ActionType.NONE);
        mainHandler.doAction(
                world,
                new MyMove(move, world),
                calcResourcesToMakeNextMove());
        updateLastMoves();

        window.update(world, army);
    }

    private double calcResourcesToMakeNextMove() {

        double additionalResources = game.getAdditionalActionCountPerControlCenter() * calcCountOfMyFacilityControlCenters();
        double resourcesToMakeNextMove = game.getBaseActionCount() + additionalResources - lastMoves.size();

        for (Integer moveIndex : lastMoves) {
            int tickDistance = game.getActionDetectionInterval() - (world.getTickIndex() - moveIndex);
            if (tickDistance > 10) {
                break;
            }
            resourcesToMakeNextMove += Math.pow(2, -tickDistance);
        }

        return resourcesToMakeNextMove;
    }

    private int calcCountOfMyFacilityControlCenters() {

        int count = 0;
        for (MyFacility facility : army.getFacilities()) {
            if (facility.getPlayerId() == world.getMyPlayer().getId() &&
                    facility.getType() == FacilityType.CONTROL_CENTER) {
                count++;
            }
        }
        return count;
    }

    private void updateLastMoves() {

        if (move.getAction() != ActionType.NONE) {
            lastMoves.add(world.getTickIndex());
        }
        if (!lastMoves.isEmpty() &&
                world.getTickIndex() - lastMoves.getFirst() >= game.getActionDetectionInterval() - 1) {
            lastMoves.pop();
        }
    }
}
