class Ellipse {

    private Pos center;
    private Vector2D v1;
    private Vector2D v2;
    private double r1;
    private double r2;


    public Pos getCenter() {
        return center;
    }

    public Vector2D getV1() {
        return v1;
    }

    public Vector2D getV2() {
        return v2;
    }

    public double getR1() {
        return r1;
    }

    public double getR2() {
        return r2;
    }

    void setV1(Vector2D v1) {
        this.v1 = v1;
    }

    void setV2(Vector2D v2) {
        this.v2 = v2;
    }

    void setCenter(Pos center) {
        this.center = center;
    }

    void setR1(double r1) {
        this.r1 = r1;
    }

    void setR2(double r2) {
        this.r2 = r2;
    }

    double getRadiusForDirection(Vector2D direction) {

        if (r1 == 0 && r2 == 0) {
            return 0;
        }

        double cosA = v1.dotprod(direction.norm());
        double sinA = Math.sqrt(1 - cosA * cosA);

        return  r1 * r2 / Math.sqrt(r1 * r1 * sinA * sinA + r2 * r2 * cosA * cosA);
    }
}
