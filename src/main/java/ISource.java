/**
 * Имплементирующий класс является источником
 */
interface ISource {

    double calcPotential(long playerId, Battalion battalion, Pos target);
}
